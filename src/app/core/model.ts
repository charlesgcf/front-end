export class Telefone {
  codigo: number;
  telefone: string;
  tipo: string;
}

export class Email {
  codigo: number;
  email: string;
}

export class Endereco {
  logradouro: string = "";
  numero: string = "";
  complemento: string = "";
  bairro: string = "";
  cep: string = "";
  cidade: string = "";
  uf: string = "";
  
}
export class EnderecoViaCep{
    bairro: string;
    cep: string = "";
    complemento: string;
    ddd: string;
    gia: string;
    ibge: string;
    localidade: string;
    logradouro: string;
    siafi: string;
    uf: string;
    erro: boolean = false;
}

export class Cliente {
  codigo: number;
  nome: string;
  cpf: string;
  endereco = new Endereco();
  telefones = Array(new Telefone());
  emails = Array(new Email());
}
