import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

import { Cliente, EnderecoViaCep } from '../core/model';

export class ClienteFiltro {
  nome: string;
  pagina = 0;
  itensPorPagina = 5;
}

@Injectable()
export class ClienteService {

  clientesUrl = 'http://localhost:8080/clientes';

  constructor(private http: HttpClient) { }

  pesquisar(filtro: ClienteFiltro): Promise<any> {
    const headers = new HttpHeaders()
      .append('Authorization', 'Basic YWRtaW5AYWxnYW1vbmV5LmNvbTphZG1pbg==');
    let params = new HttpParams()
      .set('page', filtro.pagina.toString())
      .set('size', filtro.itensPorPagina.toString());

    if (filtro.nome) {
      params = params.set('nome', filtro.nome);
    }

    return this.http.get(`${this.clientesUrl}`, { headers, params })
      .toPromise()
      .then(response => {
        const clientes = response['content'];

        const resultado = {
          clientes,
          total: response['totalElements']
        };

        return resultado;
      })
  }

  listarTodas(): Promise<any> {
    const headers = new HttpHeaders()
      .append('Authorization', 'Basic YWRtaW5AYWxnYW1vbmV5LmNvbTphZG1pbg==');

    return this.http.get(this.clientesUrl, { headers })
      .toPromise()
      .then(response => response['content']);
  }

  excluir(codigo: number): Promise<void> {
    const headers = new HttpHeaders()
      .append('Authorization', 'Basic YWRtaW5AYWxnYW1vbmV5LmNvbTphZG1pbg==');

    return this.http.delete(`${this.clientesUrl}/${codigo}`, { headers })
      .toPromise()
      .then(() => null);
  }

  mudarStatus(codigo: number, ativo: boolean): Promise<void> {
    const headers = new HttpHeaders()
      .append('Authorization', 'Basic YWRtaW5AYWxnYW1vbmV5LmNvbTphZG1pbg==')
      .append('Content-Type', 'application/json');

    return this.http.put(`${this.clientesUrl}/${codigo}/ativo`, ativo, { headers })
      .toPromise()
      .then(() => null);
  }

  adicionar(cliente: Cliente): Promise<Cliente> {
    const headers = new HttpHeaders()
      .append('Authorization', 'Basic YWRtaW5AYWxnYW1vbmV5LmNvbTphZG1pbg==')
      .append('Content-Type', 'application/json');

    return this.http.post<Cliente>(this.clientesUrl, cliente, { headers })
      .toPromise();
  }

  atualizar(cliente: Cliente): Promise<Cliente> {
    //console.log(cliente);
    const headers = new HttpHeaders()
      .append('Authorization', 'Basic YWRtaW5AYWxnYW1vbmV5LmNvbTphZG1pbg==')
      .append('Content-Type', 'application/json');

    return this.http.put<Cliente>(`${this.clientesUrl}/${cliente.codigo}`, cliente, { headers })
      .toPromise();
  }

  excluirTelefone(codigo: number): Promise<void> {
    const headers = new HttpHeaders()
      .append('Authorization', 'Basic YWRtaW5AYWxnYW1vbmV5LmNvbTphZG1pbg==');

    return this.http.delete(`${this.clientesUrl}/telefone/${codigo}`, { headers })
      .toPromise()
      .then(() => null);
  }

  excluirEmail(codigo: number): Promise<void> {
    const headers = new HttpHeaders()
      .append('Authorization', 'Basic YWRtaW5AYWxnYW1vbmV5LmNvbTphZG1pbg==');

    return this.http.delete(`${this.clientesUrl}/email/${codigo}`, { headers })
      .toPromise()
      .then(() => null);
  }

  buscarPorCodigo(codigo: number): Promise<Cliente> {
    const headers = new HttpHeaders()
      .append('Authorization', 'Basic YWRtaW5AYWxnYW1vbmV5LmNvbTphZG1pbg==');

    return this.http.get<Cliente>(`${this.clientesUrl}/${codigo}`, { headers })
      .toPromise();
  }

  consultaCEP(cep){

    if(cep !== ''){
      const validacep = /^[0-9]{8}$/;

      if(validacep.test(cep)){
        //this.resetaDadosForm();
        return this.http.get<EnderecoViaCep>(`https://viacep.com.br/ws/${cep}/json`)
          .toPromise();
      }
    }

  }

  populaDadosForm(dados){
    console.log(dados);
    // cliente.form.patchValue({
    //   endereco:{
    //     logradouro: dados,
    //     numero: null,
    //     bairro: null,
    //     cidade: null,
    //     uf: null,
    //     complemento: null,
    //   }
    // });
  }

}
