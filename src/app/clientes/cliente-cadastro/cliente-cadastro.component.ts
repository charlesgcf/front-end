import { Title } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormControl,FormBuilder, FormArray, FormGroup } from '@angular/forms';

import { ConfirmationService, LazyLoadEvent, MessageService } from 'primeng/api';

import { ErrorHandlerService } from '../../core/error-handler.service';
import { ClienteService } from '../cliente.service';
import { Cliente, Endereco, EnderecoViaCep, Telefone } from '../../core/model';
interface Tipos {
  name: string,
  value: string
}

@Component({
  selector: 'app-cliente-cadastro',
  templateUrl: './cliente-cadastro.component.html',
  styleUrls: ['./cliente-cadastro.component.css']
})


export class ClienteCadastroComponent implements OnInit {

  cliente = new Cliente();
  endViaCep = new EnderecoViaCep();
  telefones = [];
  emails = [];
  categoriaForm;

  tipos: Tipos[];
  selectedTipo: Tipos;
  tipo: any = null;
  tipos2: any[] = [
    {value:'RESIDENCIAL'},
    {value:'COMERCIAL'},
    {value:'CELULAR'},  
  ];
    

  constructor(
    private clienteService: ClienteService,
    private messageService: MessageService,
    private errorHandler: ErrorHandlerService,
    private route: ActivatedRoute,
    private confirmation: ConfirmationService,
    private router: Router,
    private title: Title,
    private readonly fb: FormBuilder
  ) { }

  ngOnInit() {
    
    const codigoCliente = this.route.snapshot.params['codigo'];

    this.title.setTitle('Nova cliente');

    if (codigoCliente) {
      this.carregarCliente(codigoCliente);
    }else{      
      this.telefones=this.cliente.telefones;
      this.emails = this.cliente.emails;
      console.log(this.emails);
    }

  }

  

  consultaCEP(event: any){
    
    var cep = event.target.value.replace(/\D+/g, '');
    if (cep != "" && cep != null  ) {
      
      this.clienteService.consultaCEP(cep)
        .then(viacep => {
          if (!viacep.erro) {
            this.viaCepParaEndereço(viacep);                        
          }else{
            this.messageService.add({ severity: 'error', detail: 'CEP não encontrado!' });
        
          }
          
          
      })
      .catch(erro => this.errorHandler.handle(erro));
    }
    
  }

  viaCepParaEndereço(viaCep: EnderecoViaCep){
    var end = new Endereco();
    end.bairro = viaCep.bairro;
    end.cep = viaCep.cep;
    end.cidade = viaCep.localidade;
    end.complemento = viaCep.complemento;
    end.logradouro = viaCep.logradouro;
    end.uf = viaCep.uf;
    this.cliente.endereco = end;     
 
 }

  carregarCliente(codigo: number) {
    this.clienteService.buscarPorCodigo(codigo)
      .then(cliente => {
        this.cliente = cliente;
        this.telefoneTipoAddArray(this.cliente.telefones);
        this.emails = this.cliente.emails;
        this.atualizarTituloEdicao();
      })
      .catch(erro => this.errorHandler.handle(erro));
  }

  telefoneTipoAddArray(telefones:any[]){
      telefones.forEach(item => {
        item.tipo={value:item.tipo}
      });
      this.telefones = telefones;
      return telefones;
  }
  telefoneTipoRemoveArray(telefones:any[]){
    telefones.forEach(item => {
      item.tipo=item.tipo.value;
     });
    return telefones;
  }
  salvar(form: FormControl) {
    if (this.editando) {
      this.atualizarCliente(form);
    } else {
      this.adicionarCliente(form);
    }
  }

  adicionarCliente(form: FormControl) { 
    this.cliente.cpf = this.cpfSemMascara;
    this.cliente.endereco.cep = this.cepSemMascara;
    this.cliente.telefones = this.telefoneSemMascara;
    this.cliente.telefones = this.telefoneTipoRemoveArray(this.cliente.telefones);
    this.clienteService.adicionar(this.cliente)
      .then(clienteAdicionada => {
        this.messageService.add({ severity: 'success', detail: 'Cliente adicionada com sucesso!' });
        
        clienteAdicionada.telefones = this.telefoneTipoAddArray(clienteAdicionada.telefones);
        this.router.navigate(['/clientes', clienteAdicionada.codigo]);
    
      })
      .catch(erro => this.errorHandler.handle(erro));
      
  }

  atualizarCliente(form: FormControl) {
    this.cliente.cpf = this.cpfSemMascara;
    this.cliente.endereco.cep = this.cepSemMascara;
    this.cliente.telefones = this.telefoneSemMascara;
    this.cliente.telefones = this.telefoneTipoRemoveArray(this.cliente.telefones);
    
    this.clienteService.atualizar(this.cliente)
      .then(cliente => {
        this.cliente = cliente;
        cliente.telefones = this.telefoneTipoAddArray(cliente.telefones);
        
        this.messageService.add({ severity: 'success', detail: 'Cliente alterada com sucesso!' });
        this.atualizarTituloEdicao();
      })
      .catch(erro => this.errorHandler.handle(erro));
  }

  nova(form: FormControl) {
    form.reset();

    setTimeout(function() {
      this.cliente = new Cliente();
    }.bind(this), 1);

    this.router.navigate(['/clientes/nova']);
  }

  atualizarTituloEdicao() {
    this.title.setTitle(`Edição de cliente: ${this.cliente.nome}`);
  }

  addTelefone() {    
    this.telefones.push({codigo: 0, telefone: '', tipo: ''});
    
  }


  confirmarExclusao(i: any) {
    this.confirmation.confirm({
      message: 'Tem certeza que deseja excluir Telefone?',
      accept: () => {
        if (this.telefones[i].codigo > 0) {
          
          this.excluirTelefone(i);
        }else{
          this.telefones.splice(i,1);
        }
      }
    });
  }

  confirmarExclusaoEmail(i: any) {
    this.confirmation.confirm({
      message: 'Tem certeza que deseja excluir Email?',
      accept: () => {
        if (this.emails[i].codigo > 0) {
          //this.emails.splice(i,1);
          this.excluirEmail(i);
        }else{
          this.emails.splice(i,1);
        }
      }
    });
  }

  excluirTelefone(i: any) {
    this.clienteService.excluirTelefone(this.cliente.telefones[i].codigo)
      .then(() => {
        this.telefones.splice(i,1);
        
        this.messageService.add({ severity: 'success', detail: 'Telefone excluída com sucesso!' });
      })
      .catch(erro => this.errorHandler.handle(erro));
  }

  excluirEmail(i: any) {
    this.clienteService.excluirEmail(this.cliente.emails[i].codigo)
      .then(() => {
        this.emails.splice(i,1);
        
        this.messageService.add({ severity: 'success', detail: 'E-mail excluída com sucesso!' });
      })
      .catch(erro => this.errorHandler.handle(erro));
  }
  

  //emails

  addEmail(){  
    this.emails.push({codigo: 0, email: ''});
      
  }

  emailVal(event:any,i){
    const emailRegex = /^[a-zA-Z0-9_.-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$/i;
    const valid = emailRegex.test(event.target.value);
    
    var element = document.getElementById("email"+i);
    if (valid) {
      element.classList.add("hide");
      element.classList.remove("show");
    }else{
      element.classList.add("show");
      element.classList.remove("hide");
    }
    
  }

  telVal(event:any,i){
    const val = event.target.value;
    console.log(val);
    var valid = val != "";
    var element = document.getElementById("tel"+i);
    if (valid) {
      element.classList.add("hide");
      element.classList.remove("show");
    }else{
      element.classList.add("show");
      element.classList.remove("hide");
    }
    
  }

  tipoVal(event:any,i){
    const val = event.target.getAttribute('aria-label');
    // console.log("val");
    // console.log(val);
    // console.log(val != " ");
    var valid = val != " ";
    var element = document.getElementById("tipo"+i);
    if (valid) {
      element.classList.add("hide");
      element.classList.remove("show");
    }else{
      element.classList.add("show");
      element.classList.remove("hide");
    }
    
  }


  get editando() {
    return Boolean(this.cliente.codigo)
  }

  get cpfSemMascara(){
    if (Object.getPrototypeOf(this.cliente.cpf)=== String.prototype) {
      return this.cliente.cpf.replace(/\D+/g, '');      
    }else{
      return this.cliente.cpf;  
    }
  }

  get cepSemMascara() {
    if (Object.getPrototypeOf(this.cliente.endereco.cep)=== String.prototype) {
      return this.cliente.endereco.cep.replace(/\D+/g, '');      
    }else{
      return this.cliente.endereco.cep;  
    }
  }  

  get telefoneSemMascara() {
    this.cliente.telefones.forEach(telefone => {
      if (Object.getPrototypeOf(telefone.telefone)=== String.prototype) {
        telefone.telefone = telefone.telefone.replace(/\D+/g, '');      
      }
    });
    return this.cliente.telefones;    
  } 

}
