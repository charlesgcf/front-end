import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ConsultaCepService {

  constructor(private http: HttpClient) { }

  consultaCEP(cep: string){
    cep = cep.replace(/\D/g,'');

    if(cep !== ''){
      const validacep = /^[0-9]{8}$/;

      if(validacep.test(cep)){
       // this.resetaDadosForm();
        return this.http.get(`//viacep.com.br/ws/${cep}/json`);
          //.subscribe(dados=>this.populaDadosForm(dados));
      }
    }

  }


}
